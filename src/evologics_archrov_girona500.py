#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
NARCIS NOTES:
* Removed USBL markers visualizaion. To be added in navigator.
* Replace direct call to recovery actions by a diadnostic message. To be
  checked in the diagnostics supervisor.
* Why the modem needs access to the nav_sts topic?
* It uses the diagnostic_helper library. To be removed?
"""

"""
@date: 25th May 2015
@author: Guillem Vallicrosa
@organization: Girona Underwater Vision and Robotics, Underwater Robotics
Research Center (CIRS), Computer Vision and Robotics Research Institute
(VICOROB), Universitat de Girona.

Adaptation for the serial modems on both vehicles to run with the COLA2
software architecture.

ID of the modems are set as follows:
  1: USBL
  2: Girona500
  3: SparusII
"""

# Basic ROS
import rospy

# Messages
from geometry_msgs.msg import PoseWithCovarianceStamped
from std_msgs.msg import Float64, Int16, String
from auv_msgs.msg import NavSts

# Services
# NARCIS: Instead of calling the recovery actions just send a diagnostic message
from cola2_msgs.srv import DigitalOutput
from cola2_msgs.srv import RecoveryAction, RecoveryActionRequest

# Custom libraries
from evologics_lib.evologics_modem_driver import EvologicsModemDriver
from evologics_lib.evologics_message import evomessage_serialize, evomessage_deserialize

# Diagnostics class
from cola2_lib.diagnostic_helper import DiagnosticHelper
# from cola2_lib import NED

from diagnostic_msgs.msg import DiagnosticStatus

from nav_msgs.msg import Odometry
from geometry_msgs.msg import PointStamped

from cola2_lib import NED

# ==============================================================================
class EvologicsROS(object):
    '''
    Class to hold all ROS related architecture on top of the pure python driver
    '''

    # ==========================================================================
    def __init__(self):
        '''
        Reads modem parameters and setups the driver with TDM.
        '''
        # Parameters
        self.name = rospy.get_name()
        self.debug = rospy.get_param('/evologics_modem/debug', False)
        address = rospy.get_param('/evologics_modem/serialtty',
                                  default='/dev/ttyS10')
        self.ownid = rospy.get_param('/evologics_modem/ownid', default=3)
        self.outid = rospy.get_param('/evologics_modem/outid', default=1)
        self.mtype = rospy.get_param('/evologics_modem/mtype', default='S')
        doutput = rospy.get_param('/evologics_modem/doutput', default=-1)
        self.owntf = rospy.get_param('/evologics_modem/tf',
                                     default=[-0.59, 0.35, -0.55])

        usbl_safe_always_on = rospy.get_param('safety/usbl_safety_always_on', default=False)
        self.error = 0
        self.nav = None
        self.lastmsg = ''
        self.target_odom = Odometry()

        # Define ned
        init_lat = rospy.get_param("navigator/ned_latitude", default=0.0)
        init_lon = rospy.get_param("navigator/ned_longitude", default=0.0)
        assert init_lat != 0.0 and init_lon != 0.0, "Error readind NED origin!"
        self.ned = NED.NED(init_lat, init_lon, 0.0)

        # Set up NED origin for publishing usbl marker
        # Narcis: The USBL markers will be drawn in the navigator
        # self.hasned = False
        # if rospy.has_param("/navigator/ned_latitude") and rospy.has_param("/navigator/ned_longitude"):
        #     self.hasned = True
        #     self.net_olat = rospy.get_param("/navigator/ned_latitude")
        #     self.net_olon = rospy.get_param("/navigator/ned_longitude")
        #     self.ned = NED.NED(self.net_olat,self.net_olon,0)

        # Set up diagnostics
        self.diagnostic = DiagnosticHelper(self.name, "soft")

        if usbl_safe_always_on:
            self.modem_age = rospy.Time.now().to_sec()
        else:
            self.modem_age = 0.0

        # Show config
        rospy.loginfo('{:s} config:\n  address: {:s}\n  ownid: {:d}\n\
  outid: {:d}\n  digital_output: {:d}'.format(self.name, address, self.ownid,
                                              self.outid, doutput))

        # Open digital output if needed
        if doutput >= 0:
            try:
                rospy.wait_for_service('digital_output', timeout=5.0)
                dout_srv = rospy.ServiceProxy('digital_output', DigitalOutput)
                dout_srv(doutput, True)
                rospy.loginfo("%s, digital_output enabled", self.name)
                rospy.sleep(3.0)
            except rospy.ServiceException, exception:
                rospy.loginfo('%s: Srv DigOut did not process request: %s',
                              self.name, str(exception))
                rospy.signal_shutdown('Cannot power Evologics modem')

        # Start the driver
        self.driver = EvologicsModemDriver(address, debug=self.debug)
        self.driver.set_id(self.ownid)

        # Start ROS Services
        try:
            rospy.wait_for_service('/cola2_safety/recovery_action', timeout=20)
            self.recovery_srv = rospy.ServiceProxy(
                '/cola2_safety/recovery_action', RecoveryAction)
        except rospy.exceptions.ROSException:
            rospy.logerr('%s, Error creating client to recovery service.',
                         self.name)
            rospy.signal_shutdown('Error creating recovery_srv client')

        # Publishers
        self.pub_to = rospy.Publisher('/cola2_communication/to_modem',
                                      String,
                                      queue_size = 2)
        self.pub_from = rospy.Publisher('/cola2_communication/from_modem',
                                        String,
                                        queue_size = 2)
        self.pub_rng = rospy.Publisher('/cola2_communication/modem_ranges',
                                       Float64,
                                       queue_size = 2)
        self.pub_usbl = rospy.Publisher('/cola2_navigation/usbl_update',
                                        PoseWithCovarianceStamped,
                                        queue_size = 2)
        self.pub_target_odom = rospy.Publisher('/cola2_communication/target_odom',
                                               Odometry,
                                               queue_size = 2)
        self.pub_target_goal = rospy.Publisher('/cola2_communication/target_goal',
                                               PointStamped,
                                               queue_size = 2)

        # Subscribers
        self.sub_nav = rospy.Subscriber('/cola2_navigation/nav_sts',
                                        NavSts, self.nav_callback)
        self.sub_error = rospy.Subscriber('/cola2_safety/error_code',
                                          Int16, self.error_callback)

    # ==========================================================================
    def error_callback(self, msg):
        '''
        Get vehicle error message status.
        @param msg: error status
        @param msg: std_msgs/Int16
        '''
        self.error = msg.data

    # ==========================================================================
    def nav_callback(self, msg):
        '''
        Get navigation status from the vehicle.
        @param msg: navigation status
        @param msg: auv_msgs/NavSts
        '''
        self.nav = msg

    # ==========================================================================
    def process_usbl(self, data):
        '''
        Publishes the received position from surface to a ROS message.

        @param data: [mtype, time, lat, lon, depth, head_acc, com_err]
        @type data: list
        '''
        # Comes from USBL and no zeros
        #print('data: ' + str(data))
        if data[0] == 'U' and data[1] != 0 and data[2] != 0:

            # Recover data
            msg = PoseWithCovarianceStamped()
            msg.header.frame_id = "/world"
            msg.header.stamp = rospy.Time(data[1])
            msg.pose.pose.position.x = data[2]
            msg.pose.pose.position.y = data[3]
            msg.pose.pose.position.z = data[4]
            msg.pose.covariance[0] = data[5]
            msg.pose.covariance[8] = data[5]
            msg.pose.covariance[16] = data[5]

            # Publish
            self.pub_usbl.publish(msg)

    # ==========================================================================
    def process_target(self, data):
        '''
        Publishes the received target goal to a ROS message.

        @param data: [mtype, time, north, east, depth, heading, speed]
        @type data: list
        '''
        # Comes from USBL and no zeros
        #print('data: ' + str(data))
        if len(data)==7 and data[0] == 'T':

            # Compete odom message with velocity and heading data
            self.target_odom.header.stamp = rospy.Time.now()
            self.target_odom.pose.pose.orientation.z = data[5]
            self.target_odom.twist.twist.linear.x = data[6]
            self.pub_target_odom.publish(self.target_odom)

            # Publish target goal
            target_goal = PointStamped()
            target_goal.header.frame_id = "/world"
            target_goal.header.stamp = rospy.Time.now()
            target_goal.point.x = data[2]
            target_goal.point.y = data[3]
            target_goal.point.z = data[4]
            self.pub_target_goal.publish(target_goal)
            print 'transponder: ', str(target_goal)
    # ==========================================================================
    def check_command(self, data):
        '''
        Checks the received command and calls the proper RecoveryAction

        @param data: deserialized message from modem
        @type data: list
        '''
        # Comes from the USBL
        if data[0] == 'U':
            if data[6] == RecoveryActionRequest.INFORMATIVE:
                pass
            elif data[6] == RecoveryActionRequest.ABORT_MISSION:
                rospy.logwarn("%s: Aborting mission", self.name)
                return self.call_recovery_srv(
                    RecoveryActionRequest.ABORT_MISSION)
            elif data[6] == RecoveryActionRequest.ABORT_AND_SURFACE:
                rospy.logwarn("%s: Abort and surface", self.name)
                return self.call_recovery_srv(
                    RecoveryActionRequest.ABORT_AND_SURFACE)
            elif data[6] == RecoveryActionRequest.EMERGENCY_SURFACE:
                rospy.logwarn("%s: Emergency surface", self.name)
                return self.call_recovery_srv(
                    RecoveryActionRequest.EMERGENCY_SURFACE)
            else:
                rospy.logerr('%s: invalid command %d from surface', self.name,
                             data[6])

    # ==========================================================================
    def iterate(self):
        '''
        Main loop to control what is received from the modem.
        '''
        # Receive data
        auv_lat = 0.0
        auv_lon = 0.0
        auv_h = 0.0

        sentences = self.driver.read()
        for sentence in sentences:
            print 'Sentence: ', sentence
            # Get IM message
            if sentence.startswith('RECVPBM'):
                vid, rng, msg = self.driver.read_recvpbm(sentence)
                if vid >= 0:
                    # Check that message changed
                    if not self.lastmsg == msg:
                        data = evomessage_deserialize(msg)
                        if data[0] == 'T':  # target message coming from sparus
                            self.process_target(data)

                        # Save as last message
                        self.lastmsg = msg

                    # Update last_modem timeout
                    self.modem_age = rospy.Time.now().to_sec()

            elif sentence.startswith('USBLLONG'):
                ret = self.driver.read_usbllong(sentence)
                if ret[0] >= 0:
                    # Compose AUV position
                    north = ret[1] + self.nav.position.north
                    east = ret[2] + self.nav.position.east
                    depth = ret[3]
                    print 'north: ', north
                    print 'east: ', east
                    print 'depth: ', depth

                    (auv_lat, auv_lon, auv_h) = self.ned.ned2geodetic([north, east, depth])

                    print 'latitude: ', auv_lat
                    print 'longitude: ', auv_lon
                    # Publish target odometry message
                    self.target_odom.header.stamp = rospy.Time.now()
                    self.target_odom.header.frame_id = '/world'
                    self.target_odom.pose.pose.position.x = north
                    self.target_odom.pose.pose.position.y = east
                    self.target_odom.pose.pose.position.z = depth
                    self.pub_target_odom.publish(self.target_odom)
                    print 'Send USBL update: \n', str(self.target_odom)
                else:
                    rospy.logwarn(self.name + ": Invalid USBLLONG sentence")

            # Output
            if len(sentence) > 0:
                self.pub_from.publish(String(sentence))

        # Send MESSAGE
        # We stored a range
        # self.pub_rng.publish(Float64(rng))

        # Information back
        val = evomessage_serialize(mtype='U',
                                   time=rospy.Time.now().to_sec(),
                                   lat=auv_lat,
                                   lon=auv_lon,
                                   depth=auv_h,
                                   head_acc=2.0,
                                   com_err=0)

        msg = self.driver.create_im(val, self.outid)
        self.driver.write(msg)
        self.pub_to.publish(String(msg))
        print 'Send: \n', msg
        # Send diagnostics message if init
        if self.modem_age > 0:
            self.diagnostic.add("last_modem_data", str(rospy.Time.now().to_sec() - self.modem_age))
            self.diagnostic.setLevel(DiagnosticStatus.OK)


    # ==========================================================================
    def call_recovery_srv(self, recovery_action):
        '''
        Call a recovery action.
        '''
        # Instead of directly calling a recovery action, sends a
        # diagnostic message to be interpreted by the diagnostics_supervisor
        self.diagnostic.add( "modem_recovery_action", str(recovery_action) )
        self.diagnostic.setLevel(DiagnosticStatus.WARN)

        req = RecoveryActionRequest()
        req.error_level = recovery_action
        try:
            self.recovery_srv(req)
            return True

        except rospy.exceptions.ROSException:
            rospy.logerr('%s, Error calling Recovery service', self.name)
            return False

# ==============================================================================
if __name__ == '__main__':

    rospy.init_node('evologics_modem_archrov_girona500')
    evologics = EvologicsROS()
    rate = rospy.Rate(0.5)
    while not rospy.is_shutdown():
        evologics.iterate()
        rate.sleep()
