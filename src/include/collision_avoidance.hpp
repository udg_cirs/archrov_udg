/*
Copyright (c) 2017, 'SRV - Universitat de les Illes Balears'
All rights reserved.

Software is provided WITHOUT WARRANTY and software authors and license owner
'SRV - Universitat de les Illes Balears' cannot be held liable for any damages.
You may use and modify this software for PRIVATE USE. REDISTRIBUTION in source
and binary forms, with or without modification, are FORBIDDEN. You may NOT use
the names, logos, or trademarks of contributors.
*/

#ifndef INCLUDE_COLA2_S2_COLLISION_AVOIDANCE_HPP
#define INCLUDE_COLA2_S2_COLLISION_AVOIDANCE_HPP

#include <ros/ros.h>
#include <auv_msgs/NavSts.h>
#include <auv_msgs/BodyVelocityReq.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <std_msgs/Float64.h>
#include <std_srvs/Empty.h>
#include <visualization_msgs/Marker.h>
#include <cola2_lib/cola2_control/Request.h>

#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl_ros/point_cloud.h>

#include <tf/transform_broadcaster.h>

#include <vector>
#include <algorithm>
#include <mutex>

typedef pcl::PointXYZ Point;
typedef pcl::PointCloud<Point> PointCloud;

inline double norm_l2(const double& a, const double& b, const double& c) {
  return sqrt(a * a + b * b + c * c);
}

inline double norm_l2(const Point& p) {
  return norm_l2(p.x, p.y, p.z);
}

inline double norm_l2(const geometry_msgs::Vector3& p) {
  return norm_l2(p.x, p.y, p.z);
}

inline double norm_l2(const geometry_msgs::Point& p) {
  return norm_l2(p.x, p.y, p.z);
}

auv_msgs::BodyVelocityReq toBodyVelocityReq(const Request& twist) {
  // Create ROS output
  auv_msgs::BodyVelocityReq output;

  ros::Time stamp;

  // Fill header
  output.header.frame_id = std::string("secure_merged_bvr");
  output.header.stamp    = stamp.fromSec(twist.getStamp());

  // Fill goal
  output.goal.priority  = twist.getPriority();
  output.goal.requester = twist.getRequester();

  // Fill disable axis
  std::vector< bool > disable_axis = twist.getDisabledAxis();
  assert(disable_axis.size() == 6);
  output.disable_axis.x     = disable_axis.at(0);
  output.disable_axis.y     = disable_axis.at(1);
  output.disable_axis.z     = disable_axis.at(2);
  output.disable_axis.roll  = disable_axis.at(3);
  output.disable_axis.pitch = disable_axis.at(4);
  output.disable_axis.yaw   = disable_axis.at(5);

  // Fill output values
  std::vector< double > values = twist.getValues();
  assert(values.size() == 6);
  output.twist.linear.x  = values.at(0);
  output.twist.linear.y  = values.at(1);
  output.twist.linear.z  = values.at(2);
  output.twist.angular.x = values.at(3);
  output.twist.angular.y = values.at(4);
  output.twist.angular.z = values.at(5);

  return output;
}

Request toRequest(const auv_msgs::BodyVelocityReq& msg) {
  Request req(msg.goal.requester,
              msg.header.stamp.toSec(),
              msg.goal.priority,
              6);

  // Set disable axis
  std::vector< bool > disabled_axis;
  disabled_axis.push_back(msg.disable_axis.x);
  disabled_axis.push_back(msg.disable_axis.y);
  disabled_axis.push_back(msg.disable_axis.z);
  disabled_axis.push_back(msg.disable_axis.roll);
  disabled_axis.push_back(msg.disable_axis.pitch);
  disabled_axis.push_back(msg.disable_axis.yaw);
  req.setDisabledAxis(disabled_axis);

  // Set values
  std::vector<double> values;
  values.push_back(msg.twist.linear.x);
  values.push_back(msg.twist.linear.y);
  values.push_back(msg.twist.linear.z);
  values.push_back(msg.twist.angular.x);
  values.push_back(msg.twist.angular.y);
  values.push_back(msg.twist.angular.z);
  req.setValues(values);

  return req;
}

/**
 * @brief      Class for collision avoidance.
 */
class CollisionAvoidance {
 public:
  /**
   * @brief      Class constructor
   */
  CollisionAvoidance() : nh_(), nh_private_("~"),
    is_nav_sts_init_(false), is_octomap_init_(false), enabled_(false) {
    // Read parameters
    node_name_ = ros::this_node::getName();
    nh_private_.param("radius", radius_, 1.0);
    nh_private_.param("priority", priority_, 10);
    nh_private_.param("max_vel", v_max_, 0.1);
    nh_private_.param("min_z_dist", min_z_dist_, 0.75);
    nh_private_.param("angular_vel_gain", angular_vel_gain_, 0.05);


    // Subscribers
    map_sub_ = nh_.subscribe("/octomap_server/octomap_point_cloud_centers", 1,
                             &CollisionAvoidance::OctomapCallback, this);
    nav_sub_ = nh_.subscribe<auv_msgs::NavSts>("/cola2_navigation/nav_sts", 20,
               &CollisionAvoidance::NavStsCallback,
               this);

    // Service to restart IBVS
    enable_srv_ = nh_private_.advertiseService("enable_collision_avoidance", &CollisionAvoidance::enable, this);

    // Service to restart IBVS
    disable_srv_ = nh_private_.advertiseService("disable_collision_avoidance", &CollisionAvoidance::disable, this);

    // Debug topics
    pub_repulsive_force_ = nh_private_.advertise<geometry_msgs::Pose>(
                             "repulsive_force", 1, true);
    pub_repulsive_force_module_ = nh_private_.advertise<std_msgs::Float64>(
                                    "repulsive_force_module", 1, true);
    pub_repulsive_force_min_v_ = nh_private_.advertise<std_msgs::Float64>(
                                   "repulsive_force_min_v", 1, true);
    pub_obstacle_points_ = nh_private_.advertise<sensor_msgs::PointCloud2>(
                             "obstacle_points", 1, true);
    pub_obstacle_distance_ = nh_private_.advertise<std_msgs::Float64>(
                               "obstacle_distance", 1, true);
    pub_security_region_ = nh_private_.advertise<visualization_msgs::Marker>(
                             "security_region", 1);
  }

  bool enable(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res) {
    enabled_ = true;
    return true;
  }

  bool disable(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res) {
    enabled_ = false;
    return true;
  }

  /**
   * @brief      Gets the navigation status.
   *
   * @return     The navigation status.
   */
  auv_msgs::NavSts getNavSts() {
    std::lock_guard<std::mutex> lock(mutex_nav_sts_);
    return nav_sts_;
  }

  /**
   * @brief      Gets the vehicle pose.
   *
   * @return     The vehicle pose.
   */
  Point getVehiclePose() {
    auv_msgs::NavSts nav_sts = getNavSts();
    return Point(nav_sts.position.north,
                 nav_sts.position.east,
                 nav_sts.position.depth);
  }

  /**
   * @brief      Gets the vehicle speed.
   *
   * @return     The vehicle speed.
   */
  double getVehicleSpeed() {
    auv_msgs::NavSts nav_sts = getNavSts();
    return norm_l2(nav_sts.body_velocity);
  }

  /**
   * @brief      Gets the vehicle orientation.
   *
   * @return     The vehicle orientation.
   */
  Point getVehicleOrientation() {
    auv_msgs::NavSts nav_sts = getNavSts();
    return Point(nav_sts.orientation.roll,
                 nav_sts.orientation.pitch,
                 nav_sts.orientation.yaw);
  }

  /**
   * @brief      Gets the point cloud.
   *
   * @return     The point cloud.
   */
  PointCloud getPointCloud() {
    std::lock_guard<std::mutex> lock(mutex_octomap_);
    return obstacle_point_cloud_;
  }

  /**
   * @brief      Sets the navigation status.
   *
   * @param[in]  nav_sts  The navigation status
   */
  void setNavSts(const auv_msgs::NavSts::ConstPtr& nav_sts) {
    std::lock_guard<std::mutex> lock(mutex_nav_sts_);
    if (!is_nav_sts_init_) {
      std::cout << "[CollisionAvoidance] NavSts received!" << std::endl;
      is_nav_sts_init_ = true;
    }
    nav_sts_ = *nav_sts;
  }

  /**
   * @brief      Sets the octomap point cloud.
   *
   * @param[in]  pc    Input point cloud
   */
  void setOctomapPointCloud(const PointCloud& pc) {
    std::lock_guard<std::mutex> lock(mutex_octomap_);
    if (!is_octomap_init_) {
      std::cout << "[CollisionAvoidance] Octomap received!" << std::endl;
      is_octomap_init_ = true;
    }
    obstacle_point_cloud_ = pc;
  }

  /**
   * @brief      Computes the new velocity body request. accumulated values for
   *             the vector sum. Vectors formed by the vehicle position and each
   *             point of the point cloud are summed to calculate the direction
   *             of the repulsion force.
   *
   * @return     The secure body velocity request
   */
  Request getSecureRequest(const Request& input_req) {
    if (!is_nav_sts_init_ || !is_octomap_init_) {
      ROS_INFO_THROTTLE(1.0, "[CollisionAvoidance] Status: NavSts received? %d Octomap received? %d", is_nav_sts_init_, is_octomap_init_);
      return input_req;
    }

    if (!enabled_) return input_req;

    PointCloud obstacle_point_cloud = getPointCloud();
    auv_msgs::BodyVelocityReq bvr = toBodyVelocityReq(input_req);
    geometry_msgs::Twist twist = bvr.twist;
    auv_msgs::NavSts nav_sts = getNavSts();

    // Determine if there is a velocity request or not
    int t1 = nav_sts.header.stamp.toSec();
    int t2 = bvr.header.stamp.toSec();
    bool no_vel_req = false;
    if (fabs(t2 - t1) > 1)
      no_vel_req = true;

    // Initialize output
    geometry_msgs::Twist out_twist = twist;
    double obstacle_dist = -1.0;

    Point cvL, cvA;
    // Do it only if there is at least one point inside the security zone.
    if (obstacle_point_cloud.size() > 0) {
      // Get vehicle pose
      Point pos_vehicle =  getVehiclePose();

      double ax = 0.0;
      double ay = 0.0;
      double az = 0.0;
      obstacle_dist = radius_;
      // take into account only points inside the security area:
      for (uint i = 0; i < obstacle_point_cloud.size(); i++) {
        // Vector from obstacle to vehicle position
        Point d(pos_vehicle.x - obstacle_point_cloud.points[i].x,
                pos_vehicle.y - obstacle_point_cloud.points[i].y,
                pos_vehicle.z - obstacle_point_cloud.points[i].z);
        double dist = norm_l2(d);
        if (dist < obstacle_dist) {
          obstacle_dist = dist;
        }
        // Add component-to-component to perform the velocity vector sum
        ax = ax + d.x;
        ay = ay + d.y;
        az = az + d.z;
      }

      // Get the repulsion direction
      double repul_v_module = norm_l2(ax, ay, az);
      Point repul_v_dir(ax / repul_v_module,
                        ay / repul_v_module,
                        az / repul_v_module);

      // Get vehicle speed
      double vehicle_speed = getVehicleSpeed();

      /* Get minimum repulsion module. If there is no velocity request,
      the minimum repulsion module will be the current robot speed.
      If there is a velocity request, the minimum velocity repulsion module
      will be the maximum between the module of the requested speed and the
      current speed */
      double v_min;
      if (no_vel_req) {
        v_min = vehicle_speed;
      } else {
        double mv = norm_l2(out_twist.linear);
        v_min = std::max(vehicle_speed, mv);
      }
      double v_max = v_max_;
      double K = ((v_min - v_max) / radius_) * obstacle_dist + v_max;
      Point repul_v_map(K * repul_v_dir.x, K * repul_v_dir.y, K * repul_v_dir.z);

      /* transform the repulsion vector referenced with respect to the /map
      coord. system into the robot system. This is necessary to be summed
      with the 'body_velocity_request', which is referenced with respect to
      the robot coord. system. */

      // Get vehicle orientation
      Point vehicle_rpy =  getVehicleOrientation();

      Point repul_v_robot = buildTransform(vehicle_rpy, repul_v_map);

      // Debug topics
      if (pub_repulsive_force_module_.getNumSubscribers() > 0) {
        std_msgs::Float64 msg;
        msg.data = K;
        pub_repulsive_force_module_.publish(msg);
      }
      if (pub_repulsive_force_min_v_.getNumSubscribers() > 0) {
        std_msgs::Float64 msg;
        msg.data = v_min;
        pub_repulsive_force_min_v_.publish(msg);
      }
      if (pub_repulsive_force_.getNumSubscribers() > 0) {
        geometry_msgs::Pose pose;
        pose.position.x = repul_v_robot.x;
        pose.position.y = repul_v_robot.y;
        pose.position.z = repul_v_robot.z;
        pub_repulsive_force_.publish(pose);
      }

      if (no_vel_req) {
        cvL = repul_v_robot;
        cvA = Point(0.0, 0.0, 0.0);  // move the robot away with no rotation
      } else {
        // case no inertial motion with close obstacle: compose the normalized
        // corrected velocity setting by adding the body velocity request with
        // respect to 'robot' with the repulsion force with respect to robot
        double a = out_twist.linear.x + repul_v_robot.x;
        double b = out_twist.linear.y + repul_v_robot.y;
        double c = out_twist.linear.z + repul_v_robot.z;
        cvL = Point(a, b, c);
        // When there is no motor in sway, then a rotation can be computed
        // using an angular velocity gain multiplied by a difference of angles
        double vel_yaw = angular_vel_gain_ * (-atan2(a, b) + M_PI / 2.0);
        cvA = Point(out_twist.angular.x, out_twist.angular.y, vel_yaw);
      }
    } else {
      // if there are no points in the security area, and the motion is not
      // inertial (there is a recent velocity request) publish this body
      // velocity request. If the motion is inertial (no vel. request) publish a
      // null request.
      if (no_vel_req) {
        cvL = Point(0.0, 0.0, 0.0);
        cvA = Point(0.0, 0.0, 0.0);
      } else {
        cvL = Point(out_twist.linear.x,
                    out_twist.linear.y,
                    out_twist.linear.z);
        cvA = Point(out_twist.angular.x,
                    out_twist.angular.y,
                    out_twist.angular.z);
      }
    }

    // Publish security region
    if (pub_security_region_.getNumSubscribers() > 0) {
      visualization_msgs::Marker marker;
      marker.header = nav_sts.header;
      marker.header.frame_id = "sparus2";
      marker.ns = "collision_avoidance";
      marker.id = 0;
      marker.type = visualization_msgs::Marker::CYLINDER;
      marker.action = visualization_msgs::Marker::MODIFY;
      marker.pose.position.x = 0.0;
      marker.pose.position.y = 0.0;
      marker.pose.position.z = 0.0;
      marker.pose.orientation.x = 0.0;
      marker.pose.orientation.y = 0.0;
      marker.pose.orientation.z = 0.0;
      marker.pose.orientation.w = 1.0;
      marker.scale.x = 2*radius_;
      marker.scale.y = 2*radius_;
      marker.scale.z = 2*min_z_dist_;
      marker.color.a = 0.5;
      marker.color.r = 0.0;
      marker.color.g = 1.0;
      marker.color.b = 0.0;
      pub_security_region_.publish(marker);
    }

    // Publish obstacle distance
    if (pub_obstacle_distance_.getNumSubscribers() > 0) {
      std_msgs::Float64 msg_obstacle_dist;
      msg_obstacle_dist.data = obstacle_dist;
      pub_obstacle_distance_.publish(msg_obstacle_dist);
    }

    // Save for output
    out_twist.linear.x = cvL.x;
    out_twist.linear.y = cvL.y;
    out_twist.linear.z = cvL.z;
    out_twist.angular.x = cvA.x;
    out_twist.angular.y = cvA.y;
    out_twist.angular.z = cvA.z;

    // Prepare output
    bvr.header.stamp = ros::Time::now();
    bvr.twist = out_twist;
    bvr.goal.requester = node_name_;
    bvr.goal.priority = 40;

    double bvr_module_linear = norm_l2(out_twist.linear);
    double bvr_module_angular = norm_l2(out_twist.angular);

    if ((bvr_module_angular != 0))
      bvr.disable_axis.yaw = false;

    Request out_req = toRequest(bvr);
    return out_req;
  }

 private:
  /**
   * @brief      Navigation status callback. Stores the x, y, z position and the
   *             speed of the robot.
   *
   * @param[in]  nav_sts  The navigation status message
   */
  void NavStsCallback(const auv_msgs::NavSts::ConstPtr& nav_sts) {
    // Set vehicle pose
    setNavSts(nav_sts);
  }

  /**
   * @brief      Catch an octomap in pointcloud2 format and store all the
   *             points in a vector to calculate the total repulsion potential
   *             field
   *
   * @param[in]  octomap_point_cloud_centers  The octomap point cloud centers
   */
  void OctomapCallback(
    const sensor_msgs::PointCloud2::ConstPtr& octomap_point_cloud_centers) {
    // Sanity check
    if (!is_nav_sts_init_) return;

    // Copy pointcloud
    PointCloud::Ptr octomap_point_cloud_centers_pcl(new PointCloud);
    fromROSMsg(*octomap_point_cloud_centers, *octomap_point_cloud_centers_pcl);

    // Get vehicle pose
    Point pos_vehicle = getVehiclePose();

    // Get obstacle points inside the region
    PointCloud obstacle_point_cloud_tmp;
    for (uint i = 0; i < octomap_point_cloud_centers_pcl->points.size(); i++) {
      Point point(octomap_point_cloud_centers_pcl->points[i]);
      Point diff(point.x - pos_vehicle.x,
                 point.y - pos_vehicle.y,
                 point.z - pos_vehicle.z);
      double distance = norm_l2(diff);
      double z_diff = point.z - pos_vehicle.z;

      // include as obstacle all those points that are inside the sphere of
      // security, above the vehicle and below it but at a distance in 'z'
      // smaller than the parameter min_z_dist_
      if ((distance < radius_) && (z_diff < min_z_dist_)) {
        obstacle_point_cloud_tmp.push_back(point);
      }
      // Exclusive access to obstacle_point_cloud_ vector
      setOctomapPointCloud(obstacle_point_cloud_tmp);
    }

    // Publish obstacle points
    if (pub_obstacle_points_.getNumSubscribers() > 0) {
      sensor_msgs::PointCloud2 out_cloud;
      pcl::toROSMsg(obstacle_point_cloud_tmp, out_cloud);
      out_cloud.header = octomap_point_cloud_centers->header;
      pub_obstacle_points_.publish(out_cloud);
    }
    return;
  }


  /**
   * @brief      returns a tf::Transform from two matrices containing a
   *             translation and a rotation
   *
   * @param[in]  p1    A vector containing the rotation in RPY
   * @param[in]  p2    A vector with the translation
   *
   * @return     The equivalent tf::Transform
   */
  Point buildTransform(const Point& p1, const Point& p2) {
    tf::Quaternion q;
    q.setRPY(p1.x, p1.y, p1.z);
    tf::Transform tf(q);
    tf::Vector3 super_vector(p2.x, p2.y, p2.z);
    tf::Vector3 transformed_vector = tf.inverse() * super_vector;

    return Point(transformed_vector.getX(),
                 transformed_vector.getY(),
                 transformed_vector.getZ());
  }

  ros::NodeHandle nh_;
  ros::NodeHandle nh_private_;

  // Parameters
  double min_z_dist_;
  double radius_;
  double v_max_;
  int priority_;
  double angular_vel_gain_;
  bool is_nav_sts_init_;
  bool is_octomap_init_;
  bool enabled_;
  std::string node_name_;

  // publishers and subscribers
  ros::Subscriber map_sub_;
  ros::Subscriber nav_sub_;
  ros::Publisher pub_repulsive_force_;
  ros::Publisher pub_repulsive_force_module_;
  ros::Publisher pub_repulsive_force_min_v_;
  ros::Publisher pub_obstacle_points_;
  ros::Publisher pub_obstacle_distance_;
  ros::Publisher pub_security_region_;

  ros::ServiceServer enable_srv_;
  ros::ServiceServer disable_srv_;

  // Vehicle pose
  std::mutex mutex_nav_sts_;
  auv_msgs::NavSts nav_sts_;

  // Obstacle points inside the radius
  PointCloud obstacle_point_cloud_;
  std::mutex mutex_octomap_;
};

#endif // INCLUDE_COLA2_S2_COLLISION_AVOIDANCE_HPP
