#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import numpy as np

# Messages
import tf
from geometry_msgs.msg import PoseWithCovarianceStamped
from cola2_msgs.msg import Detection
from cola2_lib import cola2_ros_lib
from auv_msgs.msg import NavSts


class DetectionToWorldFix():
    """Transform detection updates to USBL updates."""

    def __init__(self):
        """Constructor."""
        self.name = rospy.get_name()
        self.id_fix = 'AR_marker_0'
	self.id_object_grasp = ['AR_marker_100','AR_marker_101','AR_marker_102','AR_marker_103']
        self.auv_orientation = [0., 0., 0.]
        self.camera_tf = [0., 0., 0., 0., 0., 1.5708]
        self.landmark_position = [0., 0., 15.]
        self.get_config()
        listener = tf.TransformListener()
        print 'Read TF from ' + self.vehicle_frame + ' to ' + self.camera_frame
        try:
            listener.waitForTransform(self.vehicle_frame, self.camera_frame, rospy.Time(), rospy.Duration(4.0))
            (trans,rot) = listener.lookupTransform(self.vehicle_frame, self.camera_frame, rospy.Time(0))
            angle = tf.transformations.euler_from_quaternion([rot[0], rot[1], rot[2], rot[3]])
            self.camera_tf = [trans[0], trans[1], trans[2], angle[0], angle[1], angle[2]]
            rospy.loginfo('Camera TF: ' + str(self.camera_tf))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            rospy.logwarn("Error reading camera TF.")

        # Initialize transformations from config file
        # camera to robot (r_T_c)
        self.r_T_c = tf.transformations.euler_matrix(self.camera_tf[3],
                                                     self.camera_tf[4],
                                                     self.camera_tf[5])
        self.r_T_c[0, 3] = self.camera_tf[0]
        self.r_T_c[1, 3] = self.camera_tf[1]
        self.r_T_c[2, 3] = self.camera_tf[2]
        print 'r_T_c:\n', self.r_T_c

        # camera to robot (r_T_c)
        self.w_D_l = np.array([self.landmark_position[0],
                               self.landmark_position[1],
                               self.landmark_position[2], 1.])
        print 'w_D_l:\n', self.w_D_l

        # Publishers
        self.pub_detection = rospy.Publisher(
                                      '/cola2_navigation/detection_update',
                                      Detection,
                                      queue_size=2)
        self.pub_fix = rospy.Publisher('/cola2_navigation/world_fix_update',
                                       PoseWithCovarianceStamped,
                                       queue_size=2)
        self.pub_object_grasp = rospy.Publisher('/cola2_navigation/object_grasp',
                                       Detection,
                                       queue_size=2)
        # Subscribers
        self.sub_nav = rospy.Subscriber(
                                '/cola2_navigation/nav_sts',
                                NavSts, self.nav_callback)
        self.sub_detection = rospy.Subscriber(
                                    '/ar_marker_detections',
                                    Detection, self.detection_callback)

    def nav_callback(self, msg):
        """Save AUV orientation."""
        self.auv_orientation = [msg.orientation.roll,
                                msg.orientation.pitch,
                                msg.orientation.yaw]

    def detection_callback(self, msg):
        """Check if detection id = 0. If true publish fix, othrws detection."""
        if msg.id == self.id_fix:
            fix = PoseWithCovarianceStamped()
            fix.header = msg.header
            fix.header.frame_id = 'world'

            # Tenim w_D_l (fixe), r_T_c (fixe), c_D_l (deteccio), w_R_r (fixe)
            # Volem: w_D_r = w_D_l - w_R_r * (r_T_c * c_D_l)

            # camera to robot (r_D_c)
            c_D_l = np.array([msg.pose.pose.position.x,
                              msg.pose.pose.position.y,
                              msg.pose.pose.position.z,
                              1.0])
            # print 'c_D_l:\n', c_D_l

            w_R_r = tf.transformations.euler_matrix(self.auv_orientation[0],
                                                    self.auv_orientation[1],
                                                    self.auv_orientation[2])
            # print 'w_R_r:\n', w_R_r
            # print 'r_D_l:\n',  np.dot(self.r_T_c, c_D_l)
            # print 'w_D_l:\n',  np.dot(w_R_r, np.dot(self.r_T_c, c_D_l))

            w_D_r = self.w_D_l - np.dot(w_R_r, np.dot(self.r_T_c, c_D_l))
            # print 'w_D_r:\n', w_D_r
            fix.pose.pose.position.x = w_D_r[0]
            fix.pose.pose.position.y = w_D_r[1]
            fix.pose.pose.position.z = w_D_r[2]
            fix.pose.covariance = msg.pose.covariance
            self.pub_fix.publish(fix)
	elif msg.id in self.id_object_grasp:
            self.pub_object_grasp.publish(msg)
        else:
            self.pub_detection.publish(msg)

    def get_config(self):
        """Get config data from ros param server."""
        param_dict = {'vehicle_frame': 'detection_to_world_fix/vehicle_frame',
                      'camera_frame': 'detection_to_world_fix/camera_frame',
                      'landmark_position': 'detection_to_world_fix/landmark_position'}

        cola2_ros_lib.getRosParams(self, param_dict, self.name)


if __name__ == '__main__':
    try:
        rospy.init_node('detection_to_fix')
        detection_to_fix = DetectionToWorldFix()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
