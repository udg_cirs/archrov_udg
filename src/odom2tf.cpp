#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

class Odom2Tf {
 public:
  Odom2Tf(void) {
    ros::NodeHandle nh("~");
    nh.param("parent_id", parent_id_, std::string(""));
    nh.param("child_id", child_id_, std::string(""));
    odom_sub_ = nh.subscribe("odom", 10, &Odom2Tf::odomCallback, this);
  }
  void odomCallback(const nav_msgs::Odometry::ConstPtr& msg) {
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = msg->header.stamp;
    odom_trans.header.frame_id = parent_id_;
    odom_trans.child_frame_id = child_id_;
    odom_trans.transform.translation.x = msg->pose.pose.position.x;
    odom_trans.transform.translation.y = msg->pose.pose.position.y;
    odom_trans.transform.translation.z = msg->pose.pose.position.z;
    odom_trans.transform.rotation.x = msg->pose.pose.orientation.x;
    odom_trans.transform.rotation.y = msg->pose.pose.orientation.y;
    odom_trans.transform.rotation.z = msg->pose.pose.orientation.z;
    odom_trans.transform.rotation.w = msg->pose.pose.orientation.w;
    odom_broadcaster_.sendTransform(odom_trans);
  }
 private:
  ros::Subscriber odom_sub_;
  tf::TransformBroadcaster odom_broadcaster_;
  std::string parent_id_;
  std::string child_id_;
};

int main(int argc, char** argv){
  ros::init(argc, argv, "odom2tf");
  Odom2Tf o;
  ros::spin();
  return 0;
}