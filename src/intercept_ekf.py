#!/usr/bin/env python

import math
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PointStamped
from auv_msgs.msg import NavSts
from cola2_msgs.srv import NewGoto, NewGotoRequest, NewGotoResponse
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse
from auv_msgs.msg import GoalDescriptor
import numpy as np


import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import seaborn as sns

MAX_DISTANCE = 250.0
MIN_DISTANCE = 3.0


# Implements a linear Kalman filter.
class KalmanFilterLinear:
    def __init__(self, _A, _H, _x, _P, _Q, _R):
        self.A = _A                      # State transition matrix.
        self.H = _H                      # Observation matrix.
        self.current_state_estimate = _x # Initial state estimate.
        self.current_prob_estimate = _P  # Initial covariance estimate.
        self.Q = _Q                      # Estimated error in process.
        self.R = _R                      # Estimated error in measurements.

    def GetCurrentState(self):
        return self.current_state_estimate

    def Step(self, measurement_vector, A, R, Q):
        # print 'self.current_state_estimate: ', self.current_state_estimate
        # print 'A: ', A
        # print 'Q: ', Q
        # print 'measurement_vector: ', measurement_vector
        # print 'H: ', self.H
        # print 'R: ', R

        #---------------------------Prediction step-----------------------------
        predicted_state_estimate = A * self.current_state_estimate
        predicted_prob_estimate = (A * self.current_prob_estimate) * A.transpose() + Q
        # print 'P*: ', predicted_prob_estimate
        #--------------------------Observation step-----------------------------
        innovation = measurement_vector - self.H*predicted_state_estimate
        # print 'innovation: ', innovation
        innovation_covariance = self.H*predicted_prob_estimate*self.H.transpose() + R
        # print 'innovation_covariance: ', innovation_covariance
        #-----------------------------Update step-------------------------------
        try:
            inv_S = np.linalg.inv(innovation_covariance)
            # print 'inv: ', inv_S
        except:
            print ' ----> Singular matrix < -----'
            return -1
        # kalman_gain = predicted_prob_estimate * self.H.transpose() * np.linalg.inv(innovation_covariance)
        kalman_gain = predicted_prob_estimate * self.H.transpose() * inv_S
        # print 'Kalman gain: ', kalman_gain
        self.current_state_estimate = predicted_state_estimate + kalman_gain * innovation
        # We need the size of the matrix so we can make an identity matrix.
        size = self.current_prob_estimate.shape[0]
        # eye(n) = nxn identity matrix.
        self.current_prob_estimate = (np.eye(size)-kalman_gain*self.H)*predicted_prob_estimate
        # print 'x: ', self.current_state_estimate
        # print 'P: ', self.current_prob_estimate


class Intercept(object):
    def __init__(self, name):
        self.name = name
        self.odom = Odometry()
        self.goal = PointStamped()
        self.nav = NavSts()
        self.new_data = False
        self.usbl_position_updates = 0
        self.last_kf_update = 0.0

        # To plot
        self.north_measure = list()
        self.north_filter = list()
        self.north_USBL = list()
        self.east_measure = list()
        self.east_filter = list()
        self.east_USBL = list()
        self.stamp = list()

        # Init Service Client
        try:
            rospy.wait_for_service('/cola2_control/enable_goto', 20)
            self.enable_goto_srv = rospy.ServiceProxy(
                        '/cola2_control/enable_goto', NewGoto)
        except rospy.exceptions.ROSException:
            rospy.logerr('%s, Error creating client to enable_goto action.',
                         self.name)
            rospy.signal_shutdown('Error creating enable_goto action client')

        try:
            rospy.wait_for_service('/cola2_control/disable_goto', 20)
            self.disable_goto_srv = rospy.ServiceProxy(
                        '/cola2_control/disable_goto', Empty)
        except rospy.exceptions.ROSException:
            rospy.logerr('%s, Error creating client to disable_goto action.',
                         self.name)
            rospy.signal_shutdown('Error creating disable_goto action client')


        # Subscribers
        self.sub_target_odom = rospy.Subscriber('/cola2_communication/target_odom',
                                                Odometry, self.target_odom_callback)

        self.sub_target_goal = rospy.Subscriber('/cola2_communication/target_goal',
                                                PointStamped, self.target_goal_callback)

        self.sub_nav = rospy.Subscriber('/cola2_navigation/nav_sts',
                                        NavSts, self.nav_callback)

        # Prepare GOTO request
        self.req = NewGotoRequest()
        self.req.position.z = 0.0
        self.req.altitude_mode = False
        self.req.blocking = False
        self.req.priority = GoalDescriptor.PRIORITY_NORMAL
        self.req.reference = NewGotoRequest.REFERENCE_NED
        self.req.disable_axis.x = False
        self.req.disable_axis.y = True
        self.req.disable_axis.z = True
        self.req.disable_axis.roll = True
        self.req.disable_axis.pitch = True
        self.req.disable_axis.yaw = False
        self.req.position_tolerance.x = 2.0
        self.req.position_tolerance.y = 2.0
        self.req.position_tolerance.z = 1.0

        # Prepare to plot
        sns.set_style("whitegrid")
        sns.set_color_codes()
        self.f, self.axarr = plt.subplots(2, sharex=True)


    def initKF(self, initial_pose, error_measure):
        # No velocity model
        x = np.array([initial_pose])
        P = np.identity(1)
        A = self.createA(0.0)
        H = np.matrix([1.])
        Q = self.createQ(0.0)
        R = self.createR(error_measure)

        # Create Kalman Filter
        return KalmanFilterLinear(A, H, x, P, Q, R)

    def createA(self, dt):
        return np.matrix([1.])

    def createR(self, error_measure):
        r = np.matrix([error_measure]) # measurement error
        return r.transpose()*r

    def createQ(self, dt):
        q = np.matrix([0.1*dt]) # measurement error
        return q.transpose()*q


    def target_odom_callback(self, data):
        dt = data.header.stamp.to_sec() - self.last_kf_update
        if dt < 1.0:
            return -1

        self.odom = data
        d = self.distance(self.nav.position.north,
                          self.nav.position.east,
                          data.pose.pose.position.x,
                          data.pose.pose.position.y)

        noise = d / 4.0
        z_north = np.matrix([data.pose.pose.position.x])
        z_east = np.matrix([data.pose.pose.position.y])

        if self.usbl_position_updates == 0:
            self.KF_north = self.initKF(data.pose.pose.position.x, noise*3.0)
            self.KF_east = self.initKF(data.pose.pose.position.y, noise*3.0)
            self.init_time = data.header.stamp.to_sec()
        else:
            print 'dt: ', dt
            self.KF_north.Step(z_north,
                               self.createA(dt),
                               self.createR(noise),
                               self.createQ(dt))
            self.KF_east.Step(z_east,
                              self.createA(dt),
                              self.createR(noise),
                              self.createQ(dt))

        self.usbl_position_updates += 1
        self.last_kf_update = data.header.stamp.to_sec()

        # Save data to plot
        self.north_measure.append(z_north[0,0])
        self.east_measure.append(z_east[0,0])
        self.north_filter.append(float(self.KF_north.GetCurrentState()[0]))
        self.east_filter.append(float(self.KF_east.GetCurrentState()[0]))
        self.stamp.append((data.header.stamp.to_sec() - self.init_time)/60.0)
        self.north_USBL.append(self.nav.position.north)
        self.east_USBL.append(self.nav.position.east)

        print z_north[0,0], ', ', z_east[0,0], ', ', float(self.KF_north.GetCurrentState()[0]), ', ', float(self.KF_east.GetCurrentState()[0])
        # self.odom.pose.pose.position.x = float(self.KF.GetCurrentState()[0][0])
        # self.odom.pose.pose.position.y = float(self.KF.GetCurrentState()[1][0])
        # self.new_data = True


    def target_goal_callback(self, data):
        self.goal = data
        self.new_data = True

    def nav_callback(self, data):
        self.nav = data

        # Check if goto hasbeen achieved
        if abs(self.nav.position.north - self.req.position.x) <= MIN_DISTANCE and \
           abs(self.nav.position.east - self.req.position.y) <= MIN_DISTANCE:
               self.new_data = True
               self.req.position.x = self.req.position.x - 10.0
               self.req.position.y = self.req.position.y - 10.0


    def iterate(self):
        if self.new_data:
            # Check that the last goal & odom messages are not too old
            if self.odom != None and rospy.Time.now().to_sec() - self.odom.header.stamp.to_sec() > 30.0:
                self.odom = None
            if self.goal != None and rospy.Time.now().to_sec() - self.goal.header.stamp.to_sec() > 300.0:
                self.goal = None

            # If all the data is available...
            if self.odom != None and self.goal != None and self.nav != None:
                # Compute intercerption point

                # get vehicle mod velocity
                vel = self.odom.twist.twist.linear.x
                if self.odom.twist.twist.linear.x == 0.0:
                    vel = 0.3

                # divide v into vx and vy
                angle = math.atan2(self.goal.point.y - self.odom.pose.pose.position.y,
                                   self.goal.point.x - self.odom.pose.pose.position.x)
                print 'angle: ', angle
                vx = math.cos(angle) * vel
                vy = math.sin(angle) * vel
                print 'vx: ', vx
                print 'vy: ', vy


                t = self.intercept([self.nav.position.north, self.nav.position.east],
                                   [self.odom.pose.pose.position.x, self.odom.pose.pose.position.y, vx, vy],
                                   0.4)
                print 'Goal at ', self.goal.point.x, ',', self.goal.point.y

                if t[2] > 10.0:
                    print 'Intercep at ', t[0], ', ', t[1], ' at 0.5m/s. Intersection in ', t[2], ' seconds.'
                    d = self.distance(self.nav.position.north, self.nav.position.east, t[0], t[1])
                    if d < MAX_DISTANCE and d > MIN_DISTANCE:
                        self.req.position.x = t[0]
                        self.req.position.y = t[1]
                        self.req.linear_velocity.x = 0.5
                        self.disable_goto_srv()
                        self.enable_goto_srv(self.req)
                    else:
                        print 'Invalid distance!'

                else:
                    print 'Intercep to goal at ', self.goal.point.x, ', ',self.goal.point.y, ' at ', vel, 'm/s'
                    d = self.distance(self.nav.position.north, self.nav.position.east, self.goal.point.x, self.goal.point.y)
                    if d < MAX_DISTANCE and d > MIN_DISTANCE:
                        self.req.position.x = self.goal.point.x
                        self.req.position.y = self.goal.point.y
                        self.req.linear_velocity.x = vel
                        self.disable_goto_srv()
                        self.enable_goto_srv(self.req)
                    else:
                        print 'Invalid distance!'

            # If only target pose is available
            elif self.odom != None:
                print 'Move to target last known position: ', self.odom.pose.pose.position.x, ",", self.odom.pose.pose.position.y
                d = self.distance(self.nav.position.north, self.nav.position.east, self.odom.pose.pose.position.x, self.odom.pose.pose.position.y)
                if d < MAX_DISTANCE and d > MIN_DISTANCE:
                    self.req.position.x = self.odom.pose.pose.position.x
                    self.req.position.y = self.odom.pose.pose.position.y
                    self.req.linear_velocity.x = 0.5
                    self.disable_goto_srv()
                    self.enable_goto_srv(self.req)
                else:
                    print 'Invalid distance!'
            # If only target goal is available (Very rare)
            elif self.goal != None:
                print 'Move to target last known goal: ', self.goal.point.x, ',', self.goal.point.y
                d = self.distance(self.nav.position.north, self.nav.position.east, self.goal.point.x, self.goal.point.y)
                if d < MAX_DISTANCE and d > MIN_DISTANCE:
                    self.req.position.x = self.goal.point.x
                    self.req.position.y = self.goal.point.y
                    self.req.linear_velocity.x = 0.5
                    self.disable_goto_srv()
                    self.enable_goto_srv(self.req)
                else:
                    print 'Invalid distance!'


            self.new_data = False


    def distance(self, current_x, current_y, desired_x, desired_y):
        dist = math.sqrt((desired_x - current_x)**2 + (desired_y - current_y)**2)
        # print 'distance to target: ', dist
        return dist

    """
        Return solutions for quadratic
    """
    def quad(self, a, b, c):
        sol = None
        if abs(a) < 0.0:
            if abs(b) < 0.0:
                if abs(c) < 0.0:
                    sol = [0,0]
                else:
                    sol = None
            else:
                sol = [-c/b, -c/b]
        else:
            disc = b*b - 4*a*c
            if disc >= 0:
                disc = math.sqrt(disc)
                a = 2*a
                sol = [(-b-disc)/a, (-b+disc)/a]
            else:
                print 'Error!'
                sol = None

        return sol


    def showFigure(self):
        # plt.xlabel("time (minutes)")
        # plt.ylabel("distance (meters)")
        # plt.setp(self.ax_north, adjustable='box-forced')
        # self.fig.tight_layout()

        # Plot data
        self.t1, = self.axarr[0].plot(self.stamp, self.north_measure, 'b')
        self.t2, = self.axarr[0].plot(self.stamp, self.north_filter, 'r')
        self.t5, = self.axarr[0].plot(self.stamp, self.north_USBL, 'g')
        self.t3, = self.axarr[1].plot(self.stamp, self.east_measure, 'b')
        self.t4, = self.axarr[1].plot(self.stamp, self.east_filter, 'r')
        self.t6, = self.axarr[1].plot(self.stamp, self.east_USBL, 'g')

        plt.show()

    def intercept(self, src, dst, v):
        """
            Chaser can reach a circle x^2 + y^2 = r^2 where r = (V*t)
            Target model is:
            x = x0 + vx*t
            y = y0 + vy*t

            Then,
            * find t in (x0+vx*t)^2 + (y0+vy*t)^2 = (t*v)^2
            * fins x and y from target model
        """

        # Save param in more easy to use vars
        print 'Chaser at ', src[0], ', ', src[1], ', V:', v
        print 'Target at ', dst[0], ', ', dst[1], ' vx: ', dst[2], ' vy: ', dst[3]

        tx = dst[0] - src[0]
        ty = dst[1] - src[1]
        tvx = dst[2]
        tvy = dst[3]

        # Get quadratic equation components
        a = tvx*tvx + tvy*tvy - v*v
        b = 2 * (tvx * tx + tvy * ty)
        c = tx*tx + ty*ty

        # Solve quadratic
        ts = self.quad(a, b, c)

        # Find smallest positive solution
        if ts != None:
            t0 = ts[0]
            t1 = ts[1]
            t = min(t0, t1)
            if (t < 0):
                t = max(t0, t1)
            if (t > 0):
                x = dst[0] + tvx*t
                y = dst[1] + tvy*t
                return [x, y, t]

    def intercept2(self, src, vMag, dst, u):
        Ax = src[0]
        Ay = src[1]
        Bx = dst[0]
        By = dst[1]
        ux = u[0]
        uy = u[1]
        ABx = Bx - Ax
        ABy = By - Ay
        print 'Chaser at ', src[0], ', ', src[1], ', V:', vMag
        print 'Target at ', dst[0], ', ', dst[1], ' vx: ', u[0], ' vy: ', u[1]


        """
            Chaser can reach a circle x^2 + y^2 = r^2 where r = (V*t)
            Target model is:
            x = x0 + vx*t
            y = y0 + vy*t

            Then,
            * find t in (x0+vx*t)^2 + (y0+vy*t)^2 = (t*v)^2
            * fins x and y from target model
        """

        tmp1 = -ABx**2 * uy**2 + ABx**2 * vMag**2 + 2*ABx*ABy*ux*uy - ABy**2 * ux**2 + ABy**2* vMag**2
        tmp2 = -ABx*ux - ABy*uy
        tmp3 = ux**2 + uy**2 - vMag**2

        t1 = (-math.sqrt(tmp1) + tmp2)/tmp3
        t2 = (math.sqrt(tmp1) + tmp2)/tmp3
        if t1 < 0.0:
            t = t2
        elif t2 < 0.0:
            t = t1
        else:
            t = min(t1,t2)

        x_intercept = Bx + ux*t
        y_intercept = By + uy*t

        return [x_intercept, y_intercept, t]



# ==============================================================================
if __name__ == '__main__':

    rospy.init_node('intercept')
    intercept = Intercept(rospy.get_name())
    # Test intercept function
    # print intercept.intercept([20.0, 0.0], [10.0, 5.0, 0.2, -1.0], 1.8)
    # print intercept.intercept2([20.0, 0.0], 1.8, [10.0, 5.0], [0.2, -1.0])

    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        # intercept.iterate()
        rate.sleep()

    intercept.showFigure()
